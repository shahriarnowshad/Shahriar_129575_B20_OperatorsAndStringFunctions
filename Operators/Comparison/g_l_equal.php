<?php
$x = 100;
$y = 100;

var_dump($x >= $y); // returns true because $x is greater than or equal to $y

$x = 10;
$y = 50;

var_dump($x <= $y); // returns true because $x is less than or equal to $y

?>