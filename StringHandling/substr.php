<?php
$rest = substr("abcdef", -1);    // returns "f"
echo $rest;
echo '<br/>';
$rest = substr("abcdef", -2);    // returns "ef"
echo $rest;
echo '<br/>';
$rest = substr("abcdef", -3, 1); // returns "d"
echo $rest;

?>